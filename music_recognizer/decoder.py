import os
import fnmatch
import numpy as np
from pydub import AudioSegment
from pydub.utils import audioop
import wavio
from hashlib import sha1
from IPython import embed


def unique_hash(filepath, blocksize=2**20):
    """ Small function to generate a hash to uniquely generate
    a file. Inspired by MD5 version here:
    http://stackoverflow.com/a/1131255/712997

    Works with large files.
    """
    s = sha1()
    with open(filepath , "rb") as f:
        while True:
            buf = f.read(blocksize)
            if not buf:
                break
            s.update(buf)
    return s.hexdigest().upper()


def find_files(path, extensions):
    # Allow both with ".mp3" and without "mp3" to be used for extensions
    extensions = [e.replace(".", "") for e in extensions]

    for dirpath, dirnames, files in os.walk(path):
        for extension in extensions:
            for f in fnmatch.filter(files, "*.%s" % extension):
                p = os.path.join(dirpath, f)
                yield (p, extension)


def read(filename, start_point=None, duration=None):
    """
    Reads any file supported by pydub (ffmpeg) and returns the data contained
    within. If file reading fails due to input being a 24-bit wav file,
    wavio is used as a backup.

    Can be optionally limited to a certain amount of seconds from the start
    of the file by specifying the `limit` parameter. This is the amount of
    seconds from the start of the file.

    returns: (channels, samplerate)
    """
    # pydub does not support 24-bit wav files, use wavio when this occurs
    try:
        audiofile = AudioSegment.from_file(filename)

        if start_point:
            # limit
            end_point = (start_point + duration) * 1000
            start_point = start_point * 1000

            audiofile = audiofile[start_point:end_point]

        data = np.fromstring(audiofile._data, np.int16)

        channels = []
        for chn in xrange(audiofile.channels):
            channels.append(data[chn::audiofile.channels])

        fs = audiofile.frame_rate
    except audioop.error:
        fs, _, audiofile = wavio.readwav(filename)

        if start_point:
            # limit
            end_point = (start_point + duration) * 1000
            start_point = start_point * 1000

            audiofile = audiofile[start_point:end_point]

        audiofile = audiofile.T
        audiofile = audiofile.astype(np.int16)

        channels = []
        for chn in audiofile:
            channels.append(chn)

    return channels, audiofile.frame_rate, unique_hash(filename)


def read_string(recording, limit=None):
    fs = 44100
    nchannels = 2
    # sampwidth = 2

    # We must remove the "RIFF" chunck descriptor from recording string
    # remember there is also the "fmt" sub-chunk next
    # and the data sub-chunk after that on 44 position

    read_audio = np.fromstring(recording[44:], dtype=np.int16).reshape(-1, nchannels)
    # or use external method from wavio module shown below
    # read_audio =  wavio._wav2array(nchannels, sampwidth, recording[44:])

    if limit:
        read_audio = read_audio[:limit * 1000]

    read_audio = read_audio.T

    channels = []
    for channel in read_audio:
        channels.append(channel)

    return channels, fs


def path_to_songname(path):
    """
    Extracts song name from a filepath. Used to identify which songs
    have already been fingerprinted on disk.
    """
    return os.path.splitext(os.path.basename(path))[0]
