from __future__ import absolute_import, unicode_literals
from celery import Celery


app = Celery(
    'music_recognizer',
    broker='redis://127.0.0.1:6379/1',
    backend='redis://127.0.0.1:6379/2',
    include=[
        'music_recognizer.tasks.recognition_task'
    ]
)

app.conf.update(
    result_expires=3600,
)


if __name__ == '__main__':
    app.start()
