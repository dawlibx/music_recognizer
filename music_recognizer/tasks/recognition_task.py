from __future__ import absolute_import, unicode_literals
from music_recognizer.celery import app
from music_recognizer.recognize import MemoryRecognizer, FileRecognizer, MicrophoneRecognizer
from music_recognizer import MusicRecognizer
import json
import binascii


DEFAULT_CONFIG_FILE = 'music_recognizer.cnf'


def load_db_config(config_path):
    """
    Load config from a JSON file
    """
    try:
        with open(config_path) as f:
            config = json.load(f)
    except IOError as err:
        print("Cannot open configuration: %s. Exiting" % (str(err)))
        sys.exit(1)

    # create a MusicRecognizer instance
    return MusicRecognizer(config)


music_recognizer = load_db_config(DEFAULT_CONFIG_FILE)


@app.task
def execute(request_data):
    return music_recognizer.recognize(MemoryRecognizer, binascii.a2b_base64(request_data))
