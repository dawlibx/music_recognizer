Music Recognizer
===================================================================================================

[source venv/bin/activate]

export FLASK_APP=application.py
python -m flask run


# Celery run command:
celery -A music_recognizer worker -l info -c 4


# Fingerprint files with mp3 extension in music_files directory:
python music_recognizer.py --fingerprint music_files/ mp3
python music_recognizer.py --fingerprint music_files/ wav
python music_recognizer.py --fingerprint audiobooks_files_1/ mp3


# Operations on database:
$ mysql -u root -p
DROP DATABASE music_recognizer;
CREATE DATABASE IF NOT EXISTS music_recognizer;

# CREATE DATABASE IF NOT EXISTS speakers;

(
  mysql> show databases;
  mysql> use music_recognizer;
  mysql> drop database music_recognizer;
  mysql> truncate songs;
  mysql> truncate fingerprints;
)


######################################################################################

Suggested databases names:

music_recognizer_pairs
music_recognizer_surface_area
music_recognizer_average_value


mysql -u root -p

DROP DATABASE music_recognizer_pairs;
DROP DATABASE music_recognizer_surface_area;
DROP DATABASE music_recognizer_average_value;

CREATE DATABASE IF NOT EXISTS music_recognizer_pairs;
CREATE DATABASE IF NOT EXISTS music_recognizer_surface_area;
CREATE DATABASE IF NOT EXISTS music_recognizer_average_value;


######################################################################################

Benchmarking:

time python music_recognizer.py --fingerprint audiobooks_files_1/ mp3


######################################################################################

Profiling:

  In the code:

import cProfile
cProfile.run('foo()')

  Invoke the cProfile when running a script:

python -m cProfile music_recognizer.py --fingerprint music_files/ mp3


######################################################################################

Run all tests:

pytest tests/ -vv
(python -m pytest tests/ -vv)
