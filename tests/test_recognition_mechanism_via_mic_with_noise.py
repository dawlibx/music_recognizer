from threading import Thread
from Queue import Queue
import config
from config import mic_recognizer
import logging
import constants
from tests.helper import play_file, get_start_point, record_via_mic, save_results, summarize
from time import sleep


TEST_NAME = 'test_recognition_mechanism_via_mic_with_noise'
NOISE_FILENAME = '../noise.mp3'
DELAY = 0.5
# Play extension to be sure noise covers main music file
NOISE_PLAY_EXTENSION = 2
MUSIC_PLAY_EXTENSION = 1

logger = logging.getLogger(TEST_NAME)


def test_recognition_via_mic_with_noise():
    recorded_data = {}
    comparison_results = {}

    logger.info('Starting %s...', TEST_NAME)

    for index, filename in enumerate(constants.MUSIC_FILES_TO_RECOGNIZE):
        file_song_name = filename.rsplit('.', 1)[0]
        comparison_results[file_song_name] = {}

        logger.info('[%s] Playing with additional noise, recording and recognizing: %s', index+1, file_song_name)

        for duration in constants.DURATIONS:
            comparison_results[file_song_name][duration] = {}

            for start_point in constants.START_POINTS:
                recorded_data = {}
                playing_noise_thread = Thread(
                    target=play_file,
                    args=(NOISE_FILENAME, constants.NOISE_START_POINT, duration+NOISE_PLAY_EXTENSION,)
                )
                recording_thread = Thread(
                    target=record_via_mic,
                    args=(filename, duration, recorded_data)
                )
                playing_thread = Thread(
                    target=play_file,
                    args=(filename, start_point, duration+MUSIC_PLAY_EXTENSION,)
                )

                playing_noise_thread.start()
                playing_thread.start()
                sleep(DELAY)
                recording_thread.start()
                recording_thread.join()
                playing_thread.join()
                playing_noise_thread.join()

                recognized_recording = mic_recognizer._recognize(*recorded_data[filename])

                if not recognized_recording:
                    comparison_results[file_song_name][duration][start_point] = False

                    logger.info(
                        '[duration: %s, start_point: %s] result for %s not found: %s',
                        duration,
                        start_point,
                        filename,
                        False
                    )

                    continue

                comparison_result = recognized_recording['song_name'] == file_song_name
                comparison_results[file_song_name][duration][start_point] = comparison_result

                logger.info(
                    '[duration: %s, start_point: %s] assert result for %s == %s: %s',
                    duration,
                    start_point,
                    recognized_recording['song_name'],
                    file_song_name,
                    comparison_result
                )

    save_results(comparison_results, TEST_NAME)

    logger.info('Done with %s.', TEST_NAME)
