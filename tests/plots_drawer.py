# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import matplotlib.pyplot as plt
import constants
import os
import datetime
from IPython import embed
from results_parser import read_results_file


DECIMAL_PLACES_NUMBER = 2
X_AXIS_LABEL = 'czas trwania [s]'
Y_AXIS_LABEL = 'skuteczność [%]'
# RANGE = [0, 10, 0, 100]
SINGLE_PLOT_TYPE = '--yo'
PLOT_TYPES = ['--mo', '--bo', '--co']
PLOT_DPI = 150

# How to create plots:
# from tests import plots_drawer; plots_drawer.generate_all_plots()
# from tests import plots_drawer; plots_drawer.single_plots()
# from tests import plots_drawer; plots_drawer.generate_combined_plots()
# from tests import plots_drawer; plots_drawer.combined_fingerprinting_methods_plots()
# from tests import plots_drawer; plots_drawer.combined_recognition_types_plots()


def generate_all_plots():
    single_plots()
    generate_combined_plots()


def generate_combined_plots():
    combined_fingerprinting_methods_plots()
    combined_recognition_types_plots()


def single_plots():
    """
    Creates summary single plot for every test case with average effectiveness value for
    every duration for every song. We are summing from all given songs effectiveness values for
    given durations and we are dividing these values by number of songs to get final
    value being average effectiveness for a given duration.

    Rysuje pojedynczy wykres zaleznosci skutecznosci (effectiveness) od czasu trwania (duration).
    Wartosci skutecznosci dla kazdego czasu trwania to srednie wartosci skutecznosci dla
    kazdego czasu trwania obliczone na podstawie wynikow testow dla wybranych piosenek.
    Innymi slowy zsumowano dla kazdego czasu trwania skutecznosci ze wszystkich piosenek,
    po czym podzielono te sume poprzez ilosc piosenek otrzymujac srednia wartosc
    skutecznosci (effectiveness) dla kazdego czasu trwania (duration).
    """
    for file_name in os.listdir(constants.LOG_FILES_DIR):
        if file_name.startswith('parsed_results_'):
            results = read_results_file(file_name)
            summary_results = _summarize_results(results)
            # create lists of coordinates for x and y values
            x_axis, y_axis = _get_axes(summary_results)
            plot_title = _create_plot_title(file_name)

            _create_plot(x_axis, y_axis, plot_title)


def combined_fingerprinting_methods_plots():
    """
    1. Rysujemy wykres "rozpoznawanie z pliku" skladajacy sie z 3 wykresow przedstawiajacych kazda metode fingerprintingu.
    2. Rysujemy wykres "rozpoznawanie poprzez mikrofon" skladajacy sie z 3 wykresow przedstawiajacych kazda metode fingerprintingu.
    3. Rysujemy wykres "rozpoznawanie poprzez mikrofon z zakloceniami" skladajacy sie z 3 wykresow przedstawiajacych kazda metode fingerprintingu.
    """
    from_file_results, via_mic_results, via_mic_with_noise_results = _get_recognition_types_results()

    from_file_axes_results = _get_axes_results(from_file_results)
    via_mic_axes_results = _get_axes_results(via_mic_results)
    via_mic_with_noise_axes_results = _get_axes_results(via_mic_with_noise_results)

    _create_multiple_plot(
        'combined_fingerprinting_methods_from_file',
        from_file_axes_results,
        constants.METHODS_TRANSLATIONS
    )
    _create_multiple_plot(
        'combined_fingerprinting_methods_via_mic',
        via_mic_axes_results,
        constants.METHODS_TRANSLATIONS
    )
    _create_multiple_plot(
        'combined_fingerprinting_methods_via_mic_with_noise',
        via_mic_with_noise_axes_results,
        constants.METHODS_TRANSLATIONS
    )


def combined_recognition_types_plots():
    """
    1. Rysujemy wykres "rozpoznawanie bazujac na parze pikow" skladajacy sie z 3 wykresow przedstawiajacych kazdy typ wejscia dla rozpoznawanej piosenki (from_file, via_mic, via_mic_with_noise).
    2. Rysujemy wykres "rozpoznawanie bazujac na polu powierzchni trojkata" skladajacy sie z 3 wykresow przedstawiajacych kazdy typ wejscia dla rozpoznawanej piosenki (from_file, via_mic, via_mic_with_noise).
    3. Rysujemy wykres "rozpoznawanie bazujac na srednich wartosciach" skladajacy sie z 3 wykresow przedstawiajacych kazdy typ wejscia dla rozpoznawanej piosenki (from_file, via_mic, via_mic_with_noise).
    """
    average_results, pairs_results, surface_results = _get_fingerprinting_methods_results()

    average_results_axes_results = _get_axes_results(average_results)
    pairs_axes_results = _get_axes_results(pairs_results)
    surface_axes_results = _get_axes_results(surface_results)

    _create_multiple_plot(
        'combined_recognition_types_average',
        average_results_axes_results,
        constants.TYPES_TRANSLATIONS
    )
    _create_multiple_plot(
        'combined_recognition_types_pairs',
        pairs_axes_results,
        constants.TYPES_TRANSLATIONS
    )
    _create_multiple_plot(
        'combined_recognition_types_surface',
        surface_axes_results,
        constants.TYPES_TRANSLATIONS
    )


def _get_axes_results(results):
    axes_results = []

    for results_element in results:
        summarized_results = _summarize_results(results_element['data'])

        axes_results.append(
            {
                'title': results_element['title'],
                'axes': _get_axes(summarized_results)
            }
        )

    return axes_results


def _summarize_results(results):
    summary_results = { duration: 0 for duration in constants.DURATIONS }

    # sum effectiveness values for all duration for all the songs from results
    for song_name in results:
        for duration, effectiveness in results[song_name].iteritems():
            summary_results[int(duration)] += effectiveness

    # divide summed effectiveness values by number of songs to receive average, percentage value
    for duration, effectiveness in summary_results.iteritems():
        summary_results[duration] = int(
            round(
                summary_results[duration] / len(results),
                DECIMAL_PLACES_NUMBER
            ) * 100
        )

    return summary_results


def _get_recognition_types_results():
    from_file_results = []
    via_mic_results = []
    via_mic_with_noise_results = []

    for file_name in os.listdir(constants.LOG_FILES_DIR):
        if file_name.startswith('parsed_results_'):
            if 'from_file' in file_name:
                from_file_results.append(_results_to_append(file_name))
            elif 'via_mic_with_noise' in file_name:
                via_mic_with_noise_results.append(_results_to_append(file_name))
            elif 'via_mic' in file_name:
                via_mic_results.append(_results_to_append(file_name))

    return from_file_results, via_mic_results, via_mic_with_noise_results


def _get_fingerprinting_methods_results():
    average_results = []
    pairs_results = []
    surface_results = []

    for file_name in os.listdir(constants.LOG_FILES_DIR):
        if file_name.startswith('parsed_results_'):
            if 'average' in file_name:
                average_results.append(_results_to_append(file_name))
            elif 'pairs' in file_name:
                pairs_results.append(_results_to_append(file_name))
            elif 'surface' in file_name:
                surface_results.append(_results_to_append(file_name))

    return average_results, pairs_results, surface_results


def _results_to_append(file_name):
    return { 'title': _create_plot_title(file_name), 'data': read_results_file(file_name) }


def _get_axes(summary_results):
    x_axis = []
    y_axis = []

    for x, y in summary_results.iteritems():
        x_axis.append(x)
        y_axis.append(y)

    return x_axis, y_axis


def _create_plot(x_axis, y_axis, plot_title):
    """
    Create, save to file and show.
    """
    figure = plt.figure()
    ax = figure.add_subplot(111)

    plt.plot(x_axis, y_axis, SINGLE_PLOT_TYPE)

    for xy in zip(x_axis, y_axis):
        ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

    # plt.title(plot_title)
    # setting x axis with needed values
    plt.xticks(x_axis, x_axis)
    # plt.yticks(y_axis, y_axis)
    plt.xlabel(X_AXIS_LABEL)
    plt.ylabel(Y_AXIS_LABEL)
    plt.grid(True)

    plt.savefig(_create_plot_file_name(plot_title), dpi=PLOT_DPI, bbox_inches='tight')
    # plt.show()


def _create_multiple_plot(title, axes_results, translations):
    """
    Create, save to file and show.
    """
    figure = plt.figure()
    ax = figure.add_subplot(111)

    for axes_results_element, PLOT_TYPE in zip(axes_results, PLOT_TYPES):
        plt.plot(
            axes_results_element['axes'][0],
            axes_results_element['axes'][1],
            PLOT_TYPE,
            label=translations[axes_results_element['title']]
        )
        # add annotations to coordinates
        for xy in zip(axes_results_element['axes'][0], axes_results_element['axes'][1]):
            ax.annotate('(%s, %s)' % xy, xy=xy, textcoords='data')

        # setting x axis with needed values
        plt.xticks(
            axes_results_element['axes'][0],
            axes_results_element['axes'][0]
        )

    # plt.title()
    plt.legend(loc='best')
    plt.xlabel(X_AXIS_LABEL)
    plt.ylabel(Y_AXIS_LABEL)
    plt.grid(True)

    plt.savefig(_create_plot_file_name(title), dpi=PLOT_DPI, bbox_inches='tight')
    # plt.show()


def _create_plot_title(file_name):
    return file_name.replace('parsed_results_', '').rsplit('_2017', 2)[0]


def _create_plot_file_name(plot_title):
    timestamp = str(datetime.datetime.now().strftime(constants.STRFTIME))

    return constants.PLOTS_DIR + plot_title + timestamp + '.png'
