import random
import constants
from pydub import AudioSegment
from pydub.playback import play
from config import mic_recognizer
import datetime
import json


def play_file(filename, start_point, duration):
    if filename.rsplit('.', 1)[1] == 'wav':
        song = AudioSegment.from_wav(constants.MUSIC_FILES_DIR + filename)
    else:
        song = AudioSegment.from_mp3(constants.MUSIC_FILES_DIR + filename)

    # multiplying by 1000 because song array is in MS
    end_point = (start_point + duration) * 1000
    start_point = start_point * 1000

    play(song[start_point:end_point])


def record_via_mic(filename, duration, recorded_data):
    mic_recognizer.start_recording()

    for i in range(0, int(mic_recognizer.samplerate / mic_recognizer.chunksize * duration)):
        mic_recognizer.process_recording()

    mic_recognizer.stop_recording()

    recorded_data[filename] = mic_recognizer.data


def summarize(comparison_results):
    if False in comparison_results.values():
        logger.warning('There were unsuccessful recognition results for:')

        for file_song_name, comparison_result in comparison_results.iteritems():
            if comparison_result == False:
                logger.warning(file_song_name)
    else:
        logger.info('All files were recognized properly!')

    logger.info('Done with test_recognition_via_mic.')


def get_start_point(play_random=True):
    # rand number from 10s to 120s of recording, which is a start point of played song
    if play_random:
        return random.randint(constants.LOWER_THRESHOLD, constants.UPPER_THRESHOLD)

    return constants.LOWER_THRESHOLD

def save_results(comparison_results, test_name):
    timestamp = str(datetime.datetime.now().strftime(constants.STRFTIME))
    results_file_name = constants.LOG_FILES_DIR + test_name + timestamp + '.json'

    with open(results_file_name, 'w') as out_file:
        json.dump(comparison_results, out_file)
