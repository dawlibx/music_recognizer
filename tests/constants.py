# -*- coding: utf-8 -*-

from __future__ import unicode_literals
import os


DURATIONS = [1, 2, 3, 5, 7, 10]

START_POINTS = [10, 30, 50, 70, 90]
NOISE_START_POINT = 1
# NOISE_START_POINTS = [10, 20, 30, 40, 50]

LOWER_THRESHOLD = 10
UPPER_THRESHOLD = 120

STRFTIME = '_%Y-%m-%d_%H-%M-%S'

LOG_FILES_DIR = 'logs/'
PLOTS_DIR = 'plots/'
MUSIC_FILES_DIR = 'music_files/'
# LOG_FILES_DIR = 'logs/audiobooks/'
# PLOTS_DIR = 'plots/audiobooks/'
# MUSIC_FILES_DIR = 'audiobooks_files_1/'


# MUSIC_FILES_TO_RECOGNIZE = os.listdir(MUSIC_FILES_DIR)
# if '.DS_Store' in MUSIC_FILES_TO_RECOGNIZE:
#     MUSIC_FILES_TO_RECOGNIZE.remove('.DS_Store')

MUSIC_FILES_TO_RECOGNIZE = [
    "Anne-Marie - Ciao Adios.mp3",
    "B.B. King - Ghetto woman.mp3",
    "Bob Marley ft Lvndscape & Bolier - Is This Love.mp3",
    "Bruno Mars - Thats What I Like.mp3",
    "Calvin Harris - Slide (Feat Frank Ocean And Migos).mp3",
    "Charlie Puth - Attention.mp3",
    "David Bowie - Heroes.mp3",
    "Dreadsquad feat. Mandinka - Pirates (Modern Time Riddim).wav",
    "Ed Sheeran - Don't.mp3",
    "Ed Sheeran - Perfect.mp3",
    "John Coltrane - Giant Steps.wav",
    "Katy Perry - Chained To The Rhythm ft. Skip Marley.mp3",
    "Kendrick Lamar - HUMBLE.mp3",
    "Kygo & Ellie Goulding - First Time.mp3",
    "Little Mix - Touch.mp3",
    "Maroon 5 - Dont Wanna Know (Feat Kendrick Lamar).mp3",
    "ODB - Shimmy Shimmy Ya.wav",
    "Sam Smith - Like I Can.mp3",
    "Shawn Mendes -There's Nothing Holdin' Me Back.mp3",
    "Stevie Wonder - Sir Duke.wav",
    "Super Gravy featuring Laughton Kora & Bailey Wiley.mp3",
    "Taylor Swift - Blank Space.mp3",
    "The Chainsmokers  ft Daya - Don't Let Me Down.mp3",
    "TIEKS - Sunshine (Radio Edit).mp3",
    "Trammps - Disco Party.mp3",
    "Twenty One Pilots - Heathens.mp3",
    "You got the love - A.Skillz _Moment of my life_ edit.mp3",
    "ZAYN - Still Got Time (feat. PARTYNEXTDOOR).mp3",
    "Zara Larsson - Aint My Fault.mp3",
    "Zedd - Stay (Feat Alessia Cara).mp3"
]

# MUSIC_FILES_TO_RECOGNIZE = [
#     '(Andrzej Chyra 1) platon-obrona-sokratesa.mp3',
#     '(Andrzej Girdwoyn 1) bruno-jasienski-but-w-butonierce-tomik-miasto.mp3',
#     '(Danuta Stenka 1) 01-eliza-orzeszkowa-gloria-victis-tom-opowiadan-dziwna-historia.mp3',
#     '(Grzegorz Pawlak 1) mendele-mojcher-sforim-podroze-beniamina-trzeciego.mp3',
#     '(Iwo Verdal 1) artur-oppman-legendy-warszawskie-syrena.mp3',
#     '(Jakub Falkowski 1) ignacy-krasicki-satyry-czesc-pierwsza-do-krola.mp3',
#     '(Jan Peszek 1) franciszek-karpinski-laura-i-filon.mp3',
#     '(Joanna Domanska 1) honore-de-balzac-gobseck.mp3',
#     '(Kamila Salwerowicz 1) maria-konopnicka-nasza-szkapa_1.mp3',
#     '(Karolina Porcari 1) jan-kasprowicz-hymny-salome.mp3'
# ]


# plots_drawer module constants:

# fingerprinting methods translatons
METHODS_TRANSLATIONS = {
    'average_test_recognition_mechanism_from_file': '"średnia wartość"',
    'average_test_recognition_mechanism_via_mic': '"średnia wartość"',
    'average_test_recognition_mechanism_via_mic_with_noise': '"średnia wartość"',
    'pairs_test_recognition_mechanism_from_file': '"para pików"',
    'pairs_test_recognition_mechanism_via_mic': '"para pików"',
    'pairs_test_recognition_mechanism_via_mic_with_noise': '"para pików"',
    'surface_test_recognition_mechanism_from_file': '"pole powierzchni trójkąta"',
    'surface_test_recognition_mechanism_via_mic': '"pole powierzchni trójkąta"',
    'surface_test_recognition_mechanism_via_mic_with_noise': '"pole powierzchni trójkąta"'
}
# recognition types translatons
TYPES_TRANSLATIONS = {
    'average_test_recognition_mechanism_from_file': '"z pliku"',
    'average_test_recognition_mechanism_via_mic': '"poprzez mikrofon"',
    'average_test_recognition_mechanism_via_mic_with_noise': '"poprzez mikrofon z szumem"',
    'pairs_test_recognition_mechanism_from_file': '"z pliku"',
    'pairs_test_recognition_mechanism_via_mic': '"poprzez mikrofon"',
    'pairs_test_recognition_mechanism_via_mic_with_noise': '"poprzez mikrofon z szumem"',
    'surface_test_recognition_mechanism_from_file': '"z pliku"',
    'surface_test_recognition_mechanism_via_mic': '"poprzez mikrofon"',
    'surface_test_recognition_mechanism_via_mic_with_noise': '"poprzez mikrofon z szumem"'
}
