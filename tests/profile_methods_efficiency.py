import numpy as np
import hashlib
from operator import itemgetter
from IPython import embed
import math
import itertools


IDX_FREQ_I = 0
IDX_TIME_J = 1


######################################################################
# Degree to which a fingerprint can be paired with its neighbors --
# higher will cause more fingerprints, but potentially better accuracy.
DEFAULT_FAN_VALUE = 20

######################################################################
# Thresholds on how close or far fingerprints can be in time in order
# to be paired as a fingerprint. If your max is too low, higher values of
# DEFAULT_FAN_VALUE may not perform as expected.
MIN_HASH_TIME_DELTA = 0
MAX_HASH_TIME_DELTA = 200

######################################################################
# If True, will sort peaks temporally for fingerprinting;
# not sorting will cut down number of fingerprints, but potentially
# affect performance.
PEAK_SORT = True

######################################################################
# Number of bits to throw away from the front of the SHA1 hash in the
# fingerprint calculation. The more you throw away, the less storage, but
# potentially higher collisions and misclassifications when identifying songs.
FINGERPRINT_REDUCTION = 20

PEAKS_NUMBER = 3

EXAMPLE_LOCAL_MAXIMA = [
    (4, 9),
    (4, 36),
    (4, 115),
    (5, 88),
    (28, 14),
    (28, 81),
    (37, 56),
    (42, 3),
    (43, 106),
    (57, 22),
    (63, 70),
    (75, 41),
    (84, 29),
    (85, 77),
    (85, 103),
    (100, 1),
    (100, 64),
    (100, 109),
    (113, 76),
    (127, 103),
    (141, 26),
    (142, 78),
    (149, 115),
    (150, 1),
    (151, 59),
    (169, 21),
    (170, 103),
    (189, 53),
    (196, 18),
    (210, 112),
    (221, 48),
    (221, 70),
    (223, 17),
    (251, 17),
    (251, 68),
    (251, 102),
    (259, 48),
    (280, 19),
    (293, 111),
    (297, 4),
    (300, 58),
    (341, 15),
    (341, 94),
    (348, 70),
    (364, 18),
    (364, 110),
    (372, 94),
    (378, 53),
    (390, 14),
    (390, 110),
    (413, 94),
    (414, 52),
    (436, 67),
    (442, 14),
    (442, 110),
    (483, 57),
    (483, 110),
    (488, 4),
    (488, 31),
    (517, 94),
    (531, 15),
    (533, 41),
    (533, 84),
    (556, 4),
    (556, 67),
    (567, 14),
    (567, 41),
    (567, 94),
    (589, 19),
    (589, 41),
    (593, 94),
    (599, 0),
    (621, 41),
    (622, 84),
    (633, 107),
    (638, 14),
    (664, 57),
    (675, 107),
    (680, 14),
    (682, 41),
    (702, 110),
    (703, 57),
    (708, 84),
    (712, 14),
    (718, 67),
    (744, 50),
    (760, 110),
    (765, 4),
    (765, 57),
    (790, 50),
    (792, 4),
    (793, 110),
    (803, 73),
    (827, 50),
    (841, 4),
    (843, 107),
    (851, 72),
    (864, 51),
    (893, 106),
    (899, 50),
    (901, 14),
    (943, 110),
    (952, 50),
    (960, 9),
    (969, 107),
    (984, 50),
    (987, 99),
    (1014, 10),
    (1015, 50),
    (1028, 98),
    (1061, 98),
    (1063, 50),
    (1074, 14),
    (1090, 50),
    (1108, 4),
    (1121, 27),
    (1134, 98),
    (1135, 14),
    (1145, 67),
    (1171, 50),
    (1175, 14),
    (1177, 98),
    (1206, 27),
    (1209, 4),
    (1214, 98),
    (1216, 51),
    (1243, 98),
    (1247, 14),
    (1251, 57),
    (1267, 50),
    (1271, 98),
    (1293, 14),
    (1295, 98),
    (1298, 67),
    (1317, 50),
    (1339, 94),
    (1348, 50),
    (1355, 14),
    (1359, 107),
    (1382, 14),
    (1383, 41),
    (1383, 107),
    (1394, 94),
    (1403, 31),
    (1407, 110),
    (1409, 14),
    (1421, 67),
    (1439, 107),
    (1443, 4),
    (1446, 51),
    (1459, 67),
    (1467, 14),
    (1467, 110),
    (1482, 31),
    (1490, 73),
    (1499, 14),
    (1512, 98),
    (1522, 42),
    (1533, 15),
    (1534, 67),
    (1542, 31),
    (1557, 73),
    (1562, 98),
    (1567, 52),
    (1589, 98),
    (1606, 52),
    (1641, 94),
    (1668, 73),
    (1671, 98)
]#.sort(key=itemgetter(1))


@profile
def generate_hashes_from_pairs(peaks, fan_value=DEFAULT_FAN_VALUE):
    for i in range(len(peaks)):
        for j in range(1, fan_value):
            if (i + j) < len(peaks):
                freq1 = peaks[i][IDX_FREQ_I]
                freq2 = peaks[i + j][IDX_FREQ_I]
                t1 = peaks[i][IDX_TIME_J]
                t2 = peaks[i + j][IDX_TIME_J]
                t_delta = t2 - t1

                if t_delta >= MIN_HASH_TIME_DELTA and t_delta <= MAX_HASH_TIME_DELTA:
                    h = hashlib.sha1(
                        "%s|%s|%s" % (str(freq1), str(freq2), str(t_delta))
                    )
                    (h.hexdigest()[0:FINGERPRINT_REDUCTION], t1)


@profile
def generate_hashes_from_surface_area(peaks, fan_value=DEFAULT_FAN_VALUE):
    for i in range(len(peaks) - 1):
        for j in range(1, fan_value):
            if (i + j) < len(peaks) - 1:
                freq1 = peaks[i][IDX_FREQ_I]
                freq2 = peaks[i + j][IDX_FREQ_I]
                freq3 = peaks[i + j + 1][IDX_FREQ_I]
                t1 = peaks[i][IDX_TIME_J]
                t2 = peaks[i + j][IDX_TIME_J]
                t3 = peaks[i + j + 1][IDX_TIME_J]
                t_delta1 = t2 - t1
                t_delta2 = t3 - t2
                t_delta3 = t3 - t1
                p = 0.5 * (t_delta1 + t_delta2 + t_delta3)
                surface_area = math.sqrt(p * (p - t_delta1) * (p - t_delta2) * (p - t_delta3))

                if surface_area >= MIN_HASH_TIME_DELTA and surface_area <= MAX_HASH_TIME_DELTA:
                    h = hashlib.sha1(
                        "%s|%s|%s|%s" % (str(freq1), str(freq2), str(freq3), str(surface_area))
                    )
                    (h.hexdigest()[0:FINGERPRINT_REDUCTION], t1)

@profile
def generate_hashes_from_average_value(peaks, fan_value=DEFAULT_FAN_VALUE):
    for i in range(len(peaks) - 1):
        for j in range(1, fan_value):
            if (i + j) < len(peaks) - 1:
                freq1 = peaks[i][IDX_FREQ_I]
                freq2 = peaks[i + j][IDX_FREQ_I]
                freq3 = peaks[i + j + 1][IDX_FREQ_I]
                t1 = peaks[i][IDX_TIME_J]
                t2 = peaks[i + j][IDX_TIME_J]
                t3 = peaks[i + j + 1][IDX_TIME_J]
                t_delta1 = t2 - t1
                t_delta2 = t3 - t2
                t_delta3 = t3 - t1
                t_delta = (t_delta1 + t_delta2 + t_delta3) / 3.0

                if t_delta >= MIN_HASH_TIME_DELTA and t_delta <= MAX_HASH_TIME_DELTA:
                    h = hashlib.sha1(
                        "%s|%s|%s|%s" % (str(freq1), str(freq2), str(freq3), str(t_delta))
                    )
                    (h.hexdigest()[0:FINGERPRINT_REDUCTION], t1)


if PEAK_SORT:
    EXAMPLE_LOCAL_MAXIMA.sort(key=itemgetter(1))

@profile
def run_hashes():
    generate_hashes_from_pairs(EXAMPLE_LOCAL_MAXIMA)
    generate_hashes_from_surface_area(EXAMPLE_LOCAL_MAXIMA)
    generate_hashes_from_average_value(EXAMPLE_LOCAL_MAXIMA)

# @profile
# def run_hashes():
#     for _ in itertools.repeat(None, 1000):
#         generate_hashes_from_pairs(EXAMPLE_LOCAL_MAXIMA)
#         generate_hashes_from_surface_area(EXAMPLE_LOCAL_MAXIMA)
#         generate_hashes_from_average_value(EXAMPLE_LOCAL_MAXIMA)

run_hashes()
# import cProfile

# cProfile.run('generate_hashes_from_pairs(EXAMPLE_LOCAL_MAXIMA)')
# cProfile.run('generate_hashes_from_surface_area(EXAMPLE_LOCAL_MAXIMA)')
# cProfile.run('generate_hashes_from_average_value(EXAMPLE_LOCAL_MAXIMA)')
