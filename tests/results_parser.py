import json
import pprint
import constants
from helper import save_results
import os


def parse():
    """
    from tests.results_parser import parse; parse()
    """
    for file_name in os.listdir(constants.LOG_FILES_DIR):
        if file_name.endswith('.json'):
            results = read_results_file(file_name)
            parsed_results = _calculate_percentage_effectiveness(results)
            parsed_file_name = file_name.replace('.json', '')

            save_results(parsed_results, 'parsed_results_' + parsed_file_name)

            # pp = pprint.PrettyPrinter(indent=4)
            # pp.pprint(parsed_results)


def read_results_file(file_name):
    with open(constants.LOG_FILES_DIR + file_name) as data_file:
      results = json.load(data_file)

    return results


def _calculate_percentage_effectiveness(results):
    parsed_results = {}

    for song_name in results:
        parsed_results[song_name] = {}

        for duration in results[song_name]:
            counter = 0

            for start_point, measurement in results[song_name][duration].iteritems():
                if measurement:
                    counter += 1

            # effectiveness is a percentage value in format 0.xx
            effectiveness = counter / float(len(results[song_name][duration]))
            parsed_results[song_name][duration] = effectiveness

    return parsed_results
