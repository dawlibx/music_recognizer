import config
from config import file_recognizer
import logging
import constants
from tests.helper import save_results, summarize


TEST_NAME = 'test_recognition_mechanism_from_file'

logger = logging.getLogger(TEST_NAME)


def test_recognition_from_file():
    comparison_results = {}

    logger.info('Starting %s...', TEST_NAME)

    for index, filename in enumerate(constants.MUSIC_FILES_TO_RECOGNIZE):
        file_song_name = filename.rsplit('.', 1)[0]
        comparison_results[file_song_name] = {}

        logger.info('[%s] Recognizing from file: %s', index+1, file_song_name)

        for duration in constants.DURATIONS:
            comparison_results[file_song_name][duration] = {}

            for start_point in constants.START_POINTS:
                recognized_recording = file_recognizer.recognize_file(
                    constants.MUSIC_FILES_DIR + filename,
                    start_point,
                    duration
                )

                if not recognized_recording:
                    comparison_results[file_song_name][duration][start_point] = False

                    logger.info(
                        '[duration: %s, start_point: %s] result for %s not found: %s',
                        duration,
                        start_point,
                        filename,
                        False
                    )

                    continue

                comparison_result = recognized_recording['song_name'] == file_song_name
                comparison_results[file_song_name][duration][start_point] = comparison_result

                logger.info(
                    '[duration: %s, start_point: %s] assert result for %s == %s: %s',
                    duration,
                    start_point,
                    recognized_recording['song_name'],
                    file_song_name,
                    comparison_result
                )

    save_results(comparison_results, TEST_NAME)

    logger.info('Done with %s.', TEST_NAME)
