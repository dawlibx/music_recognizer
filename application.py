from flask import Flask, request, render_template
from music_recognizer import MusicRecognizer
from music_recognizer.recognize import MemoryRecognizer, FileRecognizer, MicrophoneRecognizer
from IPython import embed
import numpy as np
from flask import jsonify
import json
from music_recognizer.tasks import recognition_task
import binascii


app = Flask(__name__)


@app.route('/')
def index():
  return render_template('index.html')

@app.route('/recording_recognition', methods=['POST'])
def recording_recognition():
  recording = request.files.get('recording').read()

  task = recognition_task.execute.delay(binascii.b2a_base64(recording))

  return task.task_id, 202

@app.route('/recording_recognition_result', methods=['POST'])
def recording_recognition_result():
  task = recognition_task.execute.AsyncResult(request.form.get('task_id'))

  if task.ready():
      recognized_recording = task.result

      print recognized_recording

      return jsonify(recognized_recording['song_name']), 200

  return '', 204
