import os
import logging
import logging.config
import json
from music_recognizer import MusicRecognizer
from music_recognizer.recognize import FileRecognizer, MemoryRecognizer, MicrophoneRecognizer


LOGGING_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
LOGGING_LEVEL = 'INFO'
LOGGING_FILE = 'logs/music_recognizer.log'
# LOGGING_FILE = 'logs/audiobooks/music_recognizer.log'
LOGGING_PROPAGATION = True
LOGGING_VERSION = 1
LOGGING_DISABLE_EXISTING_LOGGERS = False
LOGGING_FILE_MAX_BYTES = 41943040
LOGGING_BACKUP_COUNT = 10


logging_config = {
    'version': LOGGING_VERSION,
    'disable_existing_loggers': LOGGING_DISABLE_EXISTING_LOGGERS,
    'formatters': {
        'standard': {
            'format': LOGGING_FORMAT
        },
    },
    'handlers': {
        'console': {
            'level': LOGGING_LEVEL,
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
        'file': {
            'level': LOGGING_LEVEL,
            'class':'logging.handlers.RotatingFileHandler',
            'formatter': 'standard',
            'filename': LOGGING_FILE,
            'maxBytes': LOGGING_FILE_MAX_BYTES,
            'backupCount': LOGGING_BACKUP_COUNT,
            'encoding': 'utf8'
        }
    },
    'loggers': {
        '': {
            'handlers': ['console', 'file'],
            'level': LOGGING_LEVEL,
            'propagate': LOGGING_PROPAGATION
        }
    }
}

logging.config.dictConfig(logging_config)

########################################################################################

DEFAULT_CONFIG_FILE = 'music_recognizer.cnf'

def load_db_config(config_path):
    """
    Load config from a JSON file
    """
    try:
        with open(config_path) as f:
            config = json.load(f)
    except IOError as err:
        print("Cannot open configuration: %s. Exiting" % (str(err)))
        sys.exit(1)

    # create a MusicRecognizer instance
    return MusicRecognizer(config)

music_recognizer = load_db_config(DEFAULT_CONFIG_FILE)
mic_recognizer = MicrophoneRecognizer(music_recognizer)
file_recognizer = FileRecognizer(music_recognizer)
